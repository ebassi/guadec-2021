% If you write it, they will come
% and other lies we tell ourselves
% GUADEC 2021 — Emmanuele Bassi

## Who am I?

- Emmanuele Bassi
- He/him
- GNOME, GTK

::: notes
Hi all, my lovely captive audience, and welcome to GUADEC 2021!
:::

----

## Why am I here?

::: notes
I am usually talking about technical aspects of the GNOME software development platform; this time, the technical bits are just going to be an aspect of the larger social problem of our technical documentation.

It should not come as a surprise to anybody that most of the issues a free and open source project with more than one contributor encounters are really social problems; what communities of programmers tend to do is try and program their way around the social issues, because, well: stereotypes exist for a reason. Of course, and once again surprising exactly no one, you cannot program your way out of every problem you face.

So, let's talk about documentation—and, more specifically, let's talk about developers documentation, and how the GNOME stack is faring in that regard.
:::

----

## Who am I talking to?

::: notes
Since this talk is about the technical documentation, I am not going to deal with the user documentation, which is kind of what we have been focusing in the past few years. This reduces the audience to a specific subset of the larger GNOME community, the one where "users" are actually developers.

You would think that extracting information from developers is easier than doing the same with end users, and of course you'd be wrong about it. Developers are the Worst period Users period Ever period. They suffer from Dunning Kruger just like everybody else, but crucially they live in a slightly more elevated portion of the graph, which makes them more susceptible from thinking they know more than they do, and that also makes them fall for XY problems—asking "how can I use X to do Y" and withholding what they are trying to achieve. On top of that, the hubris of a software developer, and their belief that they can understand everything regardless of context, is surpassed only by the hubris of physicists and emeritus professors.

You have to weigh the feedback coming from software developers against all these issues, in order to extract useful insight.

When talking about the developers documentation we are actually addressing two major audiences: the people who write the libraries that are part of our platform; and the people who consume the that platform to write applications. These two audiences have wildly different perspective on the documentation, not just because the former is going to write it and the latter is going to read it; but also because the former group of people has the goal of improving the project, whereas the latter group of people has the goal of getting on with their job or task at hand. Both groups will have to collaborate to ensure that the documentation gets better—if you're writing an application you cannot strictly consume an open source library maintained by volunteers as a complete black box; and on the other hand, you cannot ask application developers to become intimately familiar with the library's code base to the point of turning them into core contributors.
:::

----

## What am I talking about?

::: notes
If you ask on various places on the Internet about the developers documentation in GNOME, you'll realise that there's a fairly common response to that question: our documentation is "bad", especially when compared to other frameworks, and thus our platform is not recommended for any "serious" use.

I use "bad" in air quotes because, once you ask what makes the other framework's documentation "better", you get nebulous answers, and very, very few people scan string together a cogent point about what could be defined as "good", as opposed to "bad". What typically happens, instead, is that you get a laundry list of issues the person in question has faced. While this information *can* be useful, it's like trying to improve an application just by fixing proximate bugs of distal design issues. Yes, you end up improving things, but you never actually introduce something new, or something better. More importantly: you have no idea what end goal you want to achieve.

We iterate over every detail of our platform—from design, to API, to guidelines—and yet we never stop and ask ourselves: what are we trying to achieve with our developers documentation?

So, before we can go into some level of technical talk to outline a solution, we will have to do some theoretical framing of the question.
:::

# Part I: Cognitive surplus

----

![Clay Shirky, "Cognitive Surplus: Creativity and Generosity in a Connected Age", Penguin, 2010](./shirky-surplus.jpg)

----

> Imagine treating the free time of the world's educated citizenry as an aggregate, a kind of *cognitive surplus*. [...] One thing that makes the current age remarkable is that we can treat free time as a general social asset that can be harnessed for large, communally created projects, rather than a set of individual minutes to be whiled away one person at a time.

::: notes
First of all, I'd like to note that I am not classically trained for talking about sociology, anthropology, or psychology. I am not a humanities student, I just play one on your computer.

About 10 years ago, Clay Shirky coined the term "cognitive surplus" as a way to describe the extra resources, on a societal scale, that are unlocked by additional free time, and can be then exploited through collaborative media. A surplus is something we don't know how to use effectively: we begin by masking it, like the Gin craze of London during the early days of the Industrial Revolution, or the advent of television in the latter half of the XX century; but, once we are given enough technological capacity, we use it to create something, like the dissemination of knowledge brought by the printing press in the early 1500s, or Wikipedia, TV Tropes, or any other collaborative effort over the Internet developed in the past 30 years.

Shirky's book on this topic is full of "Internet good, old media bad" stuff, which was already naïve when not straight venturing in cherry-picking territory in 2008; and it's downright terrible in 2021. Nevertheless, the concept of cognitive surplus, of designing for generosity, and of creating civic value in collective movements, is a good descriptor of the collaborative endeavour that we know as "free and open source software".
:::

----

## Free software as cognitive surplus

::: notes
Because free software is nothing *but* cognitive surplus. We are literally coming together from all over the world to create software collaboratively, from the bottom up, and distribute to the society at large; the only barrier is having a computer and an Internet connection. Or so the marketing pamphlet they give you at the door says—in reality, there are many more barriers that are rarely examined by the people inside the community, the most important one being: creating free and open source software requires your free *time*.

Sure, you can make it your job, if you're lucky and privileged enough; but the unstated assumption is that you're going to work on these things in your spare time, as an unpaid volunteer, especially if you're young and especially if you want to make it your job in the future. Even once you have a job you may very well end up devoting your spare time to a project. Worst: it is *expected* that you're going to contribute. We routinely joke about people disappearing into corporate black holes when they get hired by large companies.

We even reached the point of companies using contributions to free and open source projects as a hiring screen; venture capitalists recommend people work on open source projects during the weekend, and gamify the GitHub activity bar; universities unleash hordes of their (barely trained) students to do resume padding onto unsuspecting communities—and, of course, all those things are expected to happen on top of the work and academic effort, thus eating into the contributor's free time.

These efforts, though, typically involve coding. Since junior coders are entirely fungible in the capitalist hellscape that is the software industry, this is to be expected; which means that non-coding roles are still left to the people that are either already inside a project, or to people that want to contribute something other than code.
:::

----

## Documentation as cognitive surplus

::: notes
Just like ogres and onions, any collaborative effort has layers. There are priorities, and goals, and structures. The stated—or more often implied—hierarchy may look flat, but the list of things to do is not. This means that there are things that get done first, and things that are done only when there's nothing else to do. Like, say: devising tests, mopping up technical debt, mentoring newcomers, or writing the documentation.

For existing members of a free software project, documentation, be it for end users or other developers, relies on additional surplus, on top of the existing surplus spent on coding or other non-coding tasks. It should not come as a surprise that the documentation is often left for last.

Of course, it would be incredibly more efficient to attract specialised contributors for writing the documentation. After all, we have people who know how to write code writing the code; people who know how to design UIs designing UIs; and people who know a specific language translating into that language. Sadly, attracting documentation writers, especially technical documentation writers, to free and open source software projects is impressively hard. In part because there is no real incentive for that to happen: people who know how to do technical writing are already doing it, and don't need to do that to get a job or burn their cognitive surplus; and in part because people in free software projects don't really advertise the need for technical writers in the first place.
:::

----

## Documentation style

::: notes
There is an additional dimension in writing documentation that is largely unexplored by people in free software projects—namely: what kind of person is writing the documentation. On one hand, you have documentation written by programmers for programmers. This type of documentation operates on the assumption that any one person who is going to read it is also going to understand it, because otherwise why would they read the documentation in the first place? *Of course* they will understand the context and jargon, and *of course* they will understand the mental process that leads from topic A to topic B; or even the constraints under which an API was designed, or the goals for which it was provided. It takes a certain lack of empathy and a certain degree of hubris to write documentation in this way, and we all know that programmers are well versed in both. This approach is not really meant for public consumption: it's a trail of bread crumbs, meant for the person who wrote the code, or has had to understand the code, and it's there for future reference.

Another approach is the "propaedeutic" one, and it's usually written by either people that have learned things in a certain way, and thus can only explain it in the same way; or by people that try to deconstruct a project's structure and code, and then reassemble it from first principles. This second approach may seem more valuable, but it's still targeted to achieve a specific task: creating more contributors. From a memetic perspective, it's the way for programs to perpetuate themselves by creating more programmers.
:::

----

## Personal vs Communal vs Civic

:::notes
The first approach is "personal": it's an effort of a single person that benefits themselves. It's a creative act, of course, but it's aimed mostly inwards. Scratching an itch by describing where the itch is and then how to scratch it.

The second approach is "communal": it's a creative effort within a community, that benefits the community.

Going back to Shirky's "cognitive surplus" theory, there's a third dimension of a creative act in social participatory models, and that is the "civic model": a creative act that benefits society at large.

The communal aspect of documentation is, by and large, a solved problem. We want to write documentation that creates more core contributors to the platform—it's the fundamental mechanism at the base of how a free and open source project can survive.

More than anything, though, we are missing the tools to ensure that we can get people to contribute to our documentation platform in a way that benefits more than newcomers to a project. Those tools are harder to design because they involve infrastructure, and that's usually the most complicated part of any project. They are harder to set up and maintain, and end up being the true bottleneck in any community, and a source of burnout for the poor souls who decide to fix the problem and end up maintaining something that very, very few people value, or even see, forever.

I think we need to spend more time in figuring out how to write our developers documentation not just to make newer versions of ourselves, but to make people use what we make without having to ship ourselves in the manner of how we ship our development environment via Docker, because that cannot scale.
:::

# Part II: Building on Ruins

----

![Ellen Ullman, "Life in Code: A Personal History of Technology", Picador USA, 2018](./ullman-life.jpg)

----

> We build our computers the way we build our cities—over time, without a plan, on top of ruins.

----

## The Past

::: notes
Now that we've outlined the problem and the question, let's get some broader context as to what we have done up until now, when it comes to documenting our development platform.

Like many things in GNOME, and free software in general, we are building on top of multiple strata of older projects. We try to improve what we had before, but often we simply have to clear out structures that have been left to decay for too long.

Each more recent part of our documentation stack is not only replacing what came before, though; it actively incorporates aspects of the past, incrementally.
:::

----

- `troff`/`nroff`/`groff`
- texinfo

::: notes
If you have never heard of these formats, then consider yourself lucky. Nevertheless, you have probably used them at least once in the past week. The first one is used for man pages, the second one is used by info pages by the GNU community. Generally speaking, you can ignore them both for newly written documentation, because nobody in their right mind would use them for anything in 2021. I'd also propose we should just ignore them altogether in the future; I know nostalgia and cargo culting your way around any problem are powerful motivators in software development, but I hope that at some point we're going to stop hitting ourselves in the face, and instead move on.

I mention these formats because part of our platform still relies on them, through a combination of inertia and fetish for a golden age that, like many other golden ages, has never really existed.

More importantly, though, the manual page infrastructure gave us a form of classification of developer documentation: each function, or family of functions, has a separate page; you have pages for API, for data structures, for file formats, and for tools. You even have pages outlining the design of entire subsystems.

Meanwhile, the texinfo pages provide a layer of cross-linking and cross-referencing that is more common for modern hyper-textual documentation.
:::

----

## The Present

::: notes
What kind of tools do we have at our disposal for building documentation, right now?
:::

----

- gtk-doc

::: notes
gtk-doc was created to move the GTK API reference out of texinfo files written by hand and into a mix of template files and comments parsed from the C source code, and that would then be turned into DocBook SGML first, and HTML last. It's about as fast as you imagine this kind of pipeline to be. Originally written in Perl, it was ported to Python as one last act of human defiance to the Elder Gods. It's by and large unmaintained, and its development basically dead in the water. Nevertheless, most of GNOME's platform is documented using it; and we still rely on its design and implementation: the annotations in our source code were defined by gtk-doc; the style conventions were established by gtk-doc; even the book-like structure of the documentation, divided in parts, chapters and appendices, is a legacy of gtk-doc.
:::

----

## The Future

::: notes
Given the dire state of disrepair of gtk-doc, though, we cannot rely on it any more—unless somebody wants to be a hero, and decides to pick it up and go through the list of issues it has.

Additionally, we now have a lot more tools we can look at when it comes to documenting our API—and, unsurprisingly, a lot of them come from other languages than C, like Python, Rust, and JavaScript.
:::

----

- gi-docgen
- hotdoc
- doxygen
- sphinx
- …

::: notes
The gi-docgen project fills out the same niche as gtk-doc, by using the introspection data instead of parsing C code *again*; it is, by and large, driven by GTK's requirements, and it does not try to be a generic, "you decide the layout and structure" documentation solution—unapologetically so, if I may add. You *can* use it, if your library follows the GNOME conventions and produces introspection data. If your library heavily relies on pre-processor macros, random collections of functions, or whatever else you do on C libraries, then I strongly encourage you to use more generic tools, like hotdoc and doxygen.

For non-API reference content, I can also recommend Sphinx. People tend to recognise the "read the docs" style from Python libraries, but it's not necessarily required to use the same theme, or publish your documentation on that platform.

In general, though, we should not make the same mistakes we made in the past, mandating that everything is done using the same tools; instead, we should pick up the right tool for the job, especially if it allows easier contribution models and faster turnaround between maintainers and documentation writers.
:::

----

## Building documentation

::: notes
Currently, building the documentation is a complex task. Using gtk-doc means depending on Docbook for style sheets, and on `xsltproc` for the processing of XML into HTML (as well as some post-processing). This makes building the API reference on platforms that are not Linux a lot more complicated, and a lot slower on every platform. We tend to gate the documentation build behind some configuration flag because, depending on the size of the documentation, it can take almost as much as the full build of the library and its ancillary bits of code, like tests, tools, and examples. Generating the documentation is typically left as an exercise for the poor souls writing the documentation, or for the maintainer, who will have to generate the API reference as part of the release process. Why is the documentation part of the release archive is an issue that we will cover later.

Switching to gi-docgen, for instance, brings the build process down to a much more tractable set of dependencies and duration; it is written in Python, and since we are already generating introspection data, that dependency is already available. The only additional dependencies are fairly well known Python packages that every distribution has, and that you can easily install with `pip`, in case you don't want to depend on distribution packages. An argument can be made about dropping the option entirely, or at the very least, enabling the API reference generation by default. This would improve the chances of the documentation being available during the development of the library itself at no additional cost, pushing contributors towards improving the reference, both in terms of content and in terms of coverage.
:::

----

## Testing

::: notes
Which brings us to the issue of testing. The existing `gtkdoc-check` tool, that very few projects actually use, when added to the test suite, can tell us whether a symbol has been documented or not. By piggybacking on the introspection data, `gi-docgen` can achieve the same using the `check` sub command. Since `gi-docgen` has access to the entire introspected API, it can perform additional checks on top of the already extensive checks performed by the introspection scanner. 

Another thing we could do is, for instance, extract code samples from the documentation, and build them as part of our test suite, in the same way `rustdoc` builds and runs embedded Rust code.
:::

----

## Installation

::: notes
All our gtk-doc based API references are installed locally under `/usr/share/gtk-doc/html`, which is somewhat questionable already. The stable release of the offline tool that lets you browse the local documentation, Devhelp, currently only looks there for references, which is even more questionable.

Since gi-docgen is not gtk-doc, it installs the generated documentation under `/usr/share/doc` instead, which is somewhat more acceptable.

There are additional considerations for installations, though: we might need to make the location configurable at build time, to allow for downstream packaging policies; and we need to install ancillary data files, like a list of additional content.
:::

----

## The local problem

::: notes
Local installations come in two kinds: the first one is globally available in your root file system, and is likely provided by your distributions. That's kind of easy to deal with, which makes cross-referencing symbols pretty trivial. We do have Devhelp files to describe symbols, and even if it's a fairly inefficient, overly verbose and yet poorly specified format, it's not hard to coerce it into giving us the ability to deal with multiple libraries. Ideally, we could move Devhelp to consume gi-docgen's index data, which is both more efficient and more exhaustive when it comes to search capabilities, as it maps to a document-oriented database fairly naturally, instead of being an XML index of symbols.

The second kind of local documentation is run times; these are isolated, by design, and they are typically only available once you run inside the sandbox. This means you can have a version of Devhelp built against GNOME 41 that reads all the API references for GNOME 41; a version of Devhelp built against GNOME 42 that reads all the API references for GNOME 42; and so on, and so forth. In theory, we could do something like GNOME Builder, and have Devhelp instantiate a sandbox for a given run time version, instead of having a specific build of Devhelp for that same version.

The obvious advantage of the second option is that the documentation applies to the run time you're using, instead of being a generic installation from your Linux distribution. This also means that you get the correct API reference for each API you have at your disposal out of the box, instead of having to know which version of GTK, GLib, WebKitGTK, etc. are shipped with GNOME 41.
:::

----

## The remote problem

::: notes
Of course, we also want to have our documentation available remotely, for reference and for answering questions. This means publishing what we build so that developers can point their web browsers to it.

With the older GNOME infrastructure that was done through a Rube Goldberg machine called library-web: the maintainer of a project would create a release archive, containing a build of the documentation; the release archive would be uploaded to a well-known location on the GNOME files server, where a daemon would take it, expand it, and figure out where the HTML and related files were; the daemon would then proceed to copy the documentation files to a location managed by a web server, and in the process it would also do some additional work, like: parsing the HTML to change all the cross-reference links from local file paths to remote URLs; tweaking the CSS to make it more consistent with the current stile of the GNOME websites; and then generate a compressed archive of the documentation.

Since we switched to GitLab, publishing API references has become a lot easier. Instead of pushing release archives to the GNOME file server, and having tools extracting it and publishing it, we can now build and publish the documentation as part of the CI pipeline that we already use for everything else. Building, testing, and publishing the documentation should really be part of the regular workflow for our libraries, just like building and publishing Flatpak bundles is part of the workflow for our applications.

Having the latest version of the API reference for every branch built alongside the rest of the code, always up to date and, more importantly, under the control of the maintainers of the API, is much more efficient.

Of course, this requires every project to modify their CI pipeline to provide this functionality. Luckily for us, GitLab supports template files, which means we can centralise the definitions in one place, and then have maintainers include that template in their own YAML file.
:::

# Part III: Bringing it all together

----

## Developer Center

::: notes
I want to make a controversial statement, here, and say that the GNOME documentation is not really "bad" *per se*. It can be better, of course, but there's nothing *inherently wrong* with it; it certainly measures up with equivalent commercial products, for something entirely volunteer driven.

The reason why it's perceived as "bad" is that our developer documentation website is actually pretty bad. It lumps all API references, with nary rhyme nor reason, into a huge list; every version is included, but it does not specify which version is available—you need to either check your Linux distribution or you need to know what run time shipped which version of whatever library. The "stable" and "unstable" links, if they even exist, tend to break, and they are relative to the current version of the library instead of the current version of the installed library or GNOME SDK, whatever that may be.

Outside of the API references, the situation is dire. There are tutorials for GTK 2 and GTK 1 still available; the guides are structured like the application help, which means they jump from topic to topic without a proper structure. There is no explanation of what libraries actually do, except a slightly outdated platform overview. More importantly: it's impossible to understand how to change things. Some guides have a link that will open your email client so that you can send a message to a mailing list; some pages are scraped from the wiki, in an attempt to make them easier to edit—but since the wiki is locked down to prevent spam, that has not really been the case for at least a decade.

In practice, our entire developer documentation story has been stuck, limited by gaps in our old infrastructure, to the point that we have been actively missing out on new contributors that cannot understand why things work they way they do under our new infrastructure; on top of that, the complexity piled upon the whole endeavour, and the fact that the infrastructore *has* been changing, led to the current haemorrhaging of existing contributors.

So, if we had to re-evaluate how to collect and present our documentation, then we should start from the ground up, and figure out what we *can* do now, instead of constantly trying to twist what we made 15 years ago for a very different infrastructure.
:::

----

## Do one thing, and do it well

::: notes
The reason why we shove as much as possible into the references is because they get built alongside our code; they get automatically uploaded; and they are easy to fix. In short: they are designed for the least possible effort, and for maximum contribution potential, just like our code.

What if our whole documentation was designed the same way?

We could have additional documentation stored in simple formats, that can even be edited straight from GitLab's web UI; tested locally without creating a Docker container that replicates our infrastructure; reviewed using merge requests; published as part of a CI pipeline; and deployed straight to a web server, without going through additional post-processing steps.

The design team has started a complete rewrite of the Human Interface Guidelines, using Sphinx as the documentation platform. Sphinx is easy to deploy locally, because it's shipped by multiple Linux distributions already, and if it isn't, it's easy to install via "pip"; you can write documentation in reStructuredText, which isn't as easy to write as plain Markdown, but at least it's well-specified, unlike Markdown; it gives you client-side search, structure, and automatic cross-linking.

The guides and tutorials we currently have on developer.gnome.org can be ported from the current tragic accident in a documentation format factory into a Sphinx-based website that will work both online and, with the addition of a simple Sphinx extension, offline through Devhelp. No more separate repositories with unclear ownership and different formats, styles, and contribution workflows.
:::

----

## Guides & Tutorials

::: notes
Right now we have multiple guides in our developer documentation: the accessibility development guide; the localisation guide for developers; the programming guidelines; and the user documentation guidelines. They exist in various states of disrepair, and need to be refreshed.

A common pain point of our developers documentation is the lack of stand alone content; we added a bunch of guides and tutorials into the API references of various projects: GLib has *two* tutorials explaining the type system, instance life cycles, properties, and signals; GTK has a whole "getting started" guide that includes examples that get built alongside the library. Very few people see them, and to be fair they should live in a more suitable place, so that they can be highlighted and indexed by search engines.

The tutorials are in even worse state. The "Tutorials" section is near the bottom of the page, and contains two links: the GTK2 tutorial and a link to an external website that returns error code 504, Gateway Timeout—and if you look at the URL in the Wayback Machine, it'll tell you that it's another GTK2 tutorial. The other tutorial on developer.gnome.org is the Python/GTK3 tutorial, way up in the list, right below the Human Interface Guidelines. Why? Who knows.

There are no links to the documentation for other languages, except a couple of low level C++ bindings API at the very bottom of the list.

There are manuals for development-related applications that should really not be here at all. There's a link for writing Metacity themes, and a guide for porting your application from GNOME 2 to GNOME 3.

And then there are the "how do I"s.
:::

----

## How do I…?

::: notes
The "How Do I" tutorials were an attempt from 2013 at writing high level tutorials with a low barrier of entry; highly focused, meant for people already at least invested in writing applications for GNOME. In order to increase the number of contributors, avoid Git and other developer tools for some reason, we used the wiki as the canonical source, with a rule that screen scraped the HTML from wiki.gnome.org and dropped it into developer.gnome.org, with some custom style. Sadly, we stumbled at the first roadblock: the wiki has been locked down to avoid constant spam waves, for the best part of the past 10 years. This requires people to join our chat—which means IRC—and ask for permission to edit the HowDoI pages. On top of that, wiki formats are by their very nature impossible to interoperate with; hence, the screen scraping.

Once again, a workaround for our previous fragmented and non-standard infrastructure ended up holding us back.

In the end, the HowDoI experiment was not a complete failure; we have interesting documentation now—albeit all over the place and impossible to index or find, unless you already know where it is, and you're already inside the community. Those short tutorials should be moved to the same location as the rest of our guides, as they are relevant to all developers, new and old alike.
:::

----

## References

::: notes
This leaves the last major source of content for the developers documentation: the API references.

If every library starts generating their API reference as part of their CI pipeline, all we need is a list of links—populated from the list of modules in our SDK—to those projects.

Problem solved? Well, yes and no. We still want to have a complete set of built API references for each version of our SDK; for local installations we do have the `org.gnome.Sdk.Docs` extension that we can publish both on our infrastructure and on Flathub; but for the remote case we do want to take that content and publish it online. The main difference with the existing developer.gnome.org layout would be that we group not by library and by library version, but by GNOME SDK version and then by library. If you are using GNOME 41, you will get the documentation for the version of GTK that shipped with GNOME 41.

This last step is probably the most complex, as we can build everything pretty easily through the same meta-project we used to generate the run times and VMs, but we need to extract the documentation and then post-process it so that we can replace the various links relative to a file system within the run time sandbox with links relative to the web server's root.
:::

----

## Less (documentation) is more (documentation)

::: notes
Once we solved the build, we need to look at the content of our API references as well.

Our API references would benefit from not having to deal with repetitive things that are actually described by the API itself. If an argument can be `NULL`, then the documentation should say it—and it should say it in a consistent way. But we do have metadata that encodes that information. In the same way, we have metadata that says that an argument is of a certain type; or whether the ownership of the data is transferred between caller and callee. We don't need that information in the source text of the documentation: we already have it in the metadata. This means that a lot of our documentation should concentrate on describing how and why, as opposed to what and where. Side effects are more important than re-iterating the type of an argument; examples are more important than explaining that a method will change a property.

We should delegate more information to the metadata associated with our API, as it will also benefit language bindings, and have the documentation tool generate well-known, consistent output from that metadata. This will free us from the boring bits of the job, and lead us towards improving the content that cannot be gleaned simply by looking at the API.
:::

----

## Documentation guidelines

::: notes
This brings us to the topic of having project-wide documentation guidelines, in the same vein as our coding style guidelines or the human interface guidelines.

Did you know that we have not one, but *two* documentation style guidelines? I would not hold it against you if you didn't, since they both live at the very bottom of the "guides" section of the current development website—and they both refer to user documentation, as opposed to developer documentation. Plus, both have outdated content from over 10 years ago.

There are actually no guides, or style guidelines, on how to write developer documentation, which is why our documentation is all over the place. Maintainers generally end up copying the style of GLib and GTK, except that neither of those projects have a consistent style either, as they have been documented by different people over a period of 20 years.

Just like we grew our coding style guidelines out of the extant coding styles, we should commit to a proper set of guidelines for writing the documentation.
:::

----

## Who are we building this for?

::: notes
Finally, I'd like to leave you with a question on how to gather feedback for our documentation. We have issue trackers, we have Discourse, we have chat, and we even have social media—but we lack a way for people to tell us if they found the documentation useful; if they found what they were looking for—and how difficult it was to find it.

How do we gather feedback from our online resources? How do we get metrics that aren't trivial stuff like "how many people accessed this page"? Less obvious: how do we gather feedback from the offline resources? Can we even do that?

I don't have answers for those questions. Maybe you do. I'd be very interested to hear them, and I'm sure everyone else involved in the GNOME documentation team is eager to hear them too.
:::

----

# Questions?

# Thank you

# Resources

- [gi-docgen](https://gitlab.gnome.org/GNOME/gi-docgen)
- [Development Center](https://gitlab.gnome.org/Teams/Documentation/developer-www)
- [Human Interface Guidelines](https://gitlab.gnome.org/Teams/Design/hig-www)
- [CI templates](https://gitlab.gnome.org/GNOME/ci-templates)
- [GTK Documentation](https://docs.gtk.org)
- [libadwaita Documentation](https://gnome.pages.gitlab.gnome.org/libadwaita/)